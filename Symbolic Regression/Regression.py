import math
from gplearn.genetic import SymbolicRegressor


def function(x):
    if x > 0:
        return (1/x) + math.sin(x)
    elif x <= 0:
        return 2 * x + x * x + 3


def regression():
    x_max = 100
    x_min = -100
    difference = 0.01

    x = [[x*difference] for x in range(int(x_min/difference), int(x_max/difference))]
    y = []
    for i in range(len(x)):
        y.append(function(x[i][0]))
    # y = [function(a) for a in x[0]]
    print(len(x))
    print(len(y))

    function_set = ["add", "sub", "mul", "div", "sqrt", "log", "abs", "neg", "inv", "max", "min", "sin", "cos", "tan"]

    reg = SymbolicRegressor(
        population_size=1024,
        generations=20,
        stopping_criteria=0.0001,
        p_crossover=0.7,
        p_subtree_mutation=0.1,
        p_hoist_mutation=0.08,
        p_point_mutation=0.1,
        max_samples=0.9,
        verbose=1,
        parsimony_coefficient=0.06,
        random_state=0,
        function_set=function_set,
        metric="mean absolute error",
    )
    reg.fit(x, y)
    print(reg.score(x, y))
    print(reg._program)


if __name__ == '__main__':
    regression()
