import os
import imageio
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
import numpy as np
import os
import PIL
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.models import Sequential
from sklearn.model_selection import learning_curve
import matplotlib.pyplot as plt


def read_data(main_folder):

    vectors = []
    classes = []
    for folder in os.listdir(main_folder):
        for instance in os.listdir(main_folder + folder):
            vec = np.array(imageio.imread(main_folder + folder + "/" + instance))
            if folder == "an":
                classes.append(0)
            elif folder == "di":
                classes.append(1)
            elif folder == "fe":
                classes.append(2)
            elif folder == "ha":
                classes.append(3)
            elif folder == "ne":
                classes.append(4)
            elif folder == "sa":
                classes.append(5)
            elif folder == "su":
                classes.append(6)
            vec = vec.flatten()
            vectors.append(vec)
            # print("asdfasdf: " + str(vec))
    return vectors, classes


def mlp_classify(vectors, labels):
    train_vec, test_vec, train_label, test_label = train_test_split(vectors, labels, train_size=0.666, stratify=labels)
    average = []
    count = 1
    for i in range(count):
        nn = MLPClassifier(
            hidden_layer_sizes=(120, 70),
            # solver="lbfgs",
            max_iter=10000,
            n_iter_no_change=150
        )

        fitted_model = nn.fit(train_vec, train_label)
        # print(nn.score(test_vec, test_class))
        y_pred = fitted_model.predict(test_vec)

        accuracy = accuracy_score(test_label, y_pred)
        # print("Accuracy: " + "{:.1%}".format(float(accuracy)))
        average.append(accuracy)
        plt.plot(nn.loss_curve_)
        plt.show()
        # learning_curve(nn, train_vec, train_label)
    print("Average accuracy over " + str(count) + " runs: " + "{:.1%}".format(np.mean(average)))


# code from https://www.tensorflow.org/tutorials/images/classification
def cnn_classify():
    batch_size = 32
    img_height = 128
    img_width = 128

    train_ds = tf.keras.preprocessing.image_dataset_from_directory(
        "/home/dynehayd/PycharmProjects/Comp422Project2/Data/jaffe",
        validation_split=0.2,
        subset="training",
        seed=123,
        image_size=(img_height, img_width),
        batch_size=batch_size)
    val_ds = tf.keras.preprocessing.image_dataset_from_directory(
        "/home/dynehayd/PycharmProjects/Comp422Project2/Data/jaffe",
        validation_split=0.2,
        subset="validation",
        seed=123,
        image_size=(img_height, img_width),
        batch_size=batch_size)
    class_names = train_ds.class_names

    num_classes = 7

    model = Sequential([
        layers.experimental.preprocessing.Rescaling(1. / 255, input_shape=(img_height, img_width, 3)),
        layers.Conv2D(16, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(),
        layers.Conv2D(32, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(),
        layers.Conv2D(64, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(),
        layers.Flatten(),
        layers.Dense(128, activation='relu'),
        layers.Dense(num_classes)
    ])

    model.compile(optimizer='adam',
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  metrics=['accuracy'])

    model.summary()

    epochs = 10
    history = model.fit(
        train_ds,
        validation_data=val_ds,
        epochs=epochs
    )

    acc = history.history['accuracy']
    val_acc = history.history['val_accuracy']

    loss = history.history['loss']
    val_loss = history.history['val_loss']

    epochs_range = range(epochs)

    plt.figure(figsize=(8, 8))
    plt.subplot(1, 2, 1)
    plt.plot(epochs_range, acc, label='Training Accuracy')
    plt.plot(epochs_range, val_acc, label='Validation Accuracy')
    plt.legend(loc='lower right')
    plt.title('Training and Validation Accuracy')

    plt.subplot(1, 2, 2)
    plt.plot(epochs_range, loss, label='Training Loss')
    plt.plot(epochs_range, val_loss, label='Validation Loss')
    plt.legend(loc='upper right')
    plt.title('Training and Validation Loss')
    plt.show()

    return 0


if __name__ == '__main__':
    x, y = read_data("/home/dynehayd/PycharmProjects/Comp422Project2/Data/jaffe/")
    mlp_classify(x, y)
    cnn_classify()
