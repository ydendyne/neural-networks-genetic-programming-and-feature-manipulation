from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
import numpy as np


def load_data(file_name):
    fp = open(file_name, "r")
    input_vecs = []
    input_class = []
    for line in fp:
        str = line.split(" ")
        class_ = int(str[-1])
        vec = [int(s) for s in str]
        vec.pop()
        input_vecs.append(vec)
        input_class.append(class_)
    return input_vecs, input_class


def mlp_classify(input_vec, input_class):

    print("NEURAL NETWORK")

    average = []
    count = 10
    for i in range(count):
        nn = MLPClassifier(
            hidden_layer_sizes=(49,),
            solver="lbfgs",
            max_iter=1000
        )

        train_vec = input_vec[0:500]
        test_vec = input_vec[500:]
        train_class = input_class[0:500]
        test_class = input_class[500:]

        fitted_model = nn.fit(train_vec, train_class)
        # print(nn.score(test_vec, test_class))
        y_pred = fitted_model.predict(test_vec)

        accuracy = accuracy_score(test_class, y_pred)
        average.append(accuracy)
        # print("Accuracy: " + "{:.1%}".format(float(accuracy)))

        # print('NN Output:', nn.predict(train_vec))
    print("Average accuracy over " + str(count) + " runs: " + "{:.1%}".format(np.mean(average)))


def knn_classify(input_vec, input_class):

    print("NEAREST NEIGHBOR")

    knn = KNeighborsClassifier()

    train_vec = input_vec[0:500]
    test_vec = input_vec[500:]
    train_class = input_class[0:500]
    test_class = input_class[500:]

    fitted_model = knn.fit(train_vec, train_class)
    # print(nn.score(test_vec, test_class))
    y_pred = fitted_model.predict(test_vec)

    accuracy = accuracy_score(test_class, y_pred)
    print("Accuracy: " + "{:.1%}".format(float(accuracy)))

    # print('NN Output:', knn.predict(train_vec))


if __name__ == '__main__':
    x, y = load_data("/home/dynehayd/PycharmProjects/Comp422Project2/Data/digits/digits/digits60")
    mlp_classify(x, y)
    knn_classify(x, y)
