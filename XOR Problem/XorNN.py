import numpy as np
import sklearn.neural_network
from sklearn.metrics import accuracy_score

input_ = np.array(
    [
        [0, 0],
        [0, 1],
        [1, 0],
        [1, 1]
    ]
)

xor_output = np.array(
    [
        0,
        1,
        1,
        0
    ]
)

model = sklearn.neural_network.MLPClassifier(
                activation='logistic',
                max_iter=1000,
                hidden_layer_sizes=(2,),
                solver='lbfgs'
)

fitted_model = model.fit(input_, xor_output)

y_pred = fitted_model.predict(input_)

accuracy = accuracy_score(xor_output, y_pred)
print("Accuracy: " + "{:.1%}".format(float(accuracy)))

print('NN Output:', model.predict(input_))
