import numpy as np
from gplearn.genetic import SymbolicRegressor

input_ = np.array(
    [
        [0, 0],
        [0, 1],
        [1, 0],
        [1, 1]
    ]
)

xor_output = np.array(
    [
        0,
        1,
        1,
        0
    ]
)


regression = SymbolicRegressor(population_size=100,
                               generations=20,
                               stopping_criteria=0.0001,
                               verbose=1,
                               metric="mean absolute error",
                               function_set=["abs", "sub"]
                               )

regression.fit(input_, xor_output)
print("Accuracy: " + str(regression.score(input_, xor_output)))
print("Program: " + str(regression._program))


