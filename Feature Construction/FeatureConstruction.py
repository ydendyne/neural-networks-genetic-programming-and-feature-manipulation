import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.metrics import accuracy_score
from gplearn.genetic import SymbolicTransformer
import numpy as np


def read_data(file_name):
    if "wine" in file_name:
        vec = pd.read_csv(file_name, header=None, usecols=[i for i in range(13)])
        lab = pd.read_csv(file_name, header=None)
        lab = lab[13]
        return vec, lab
    elif "balance" in file_name:
        vec = pd.read_csv(file_name, header=None, usecols=[i for i in range(4)])
        lab = pd.read_csv(file_name, header=None)
        lab = lab[4]
        return vec, lab
    else:
        print("Incorret file")


def naive_bayes_classify(input_vec, input_lab):
    nb = GaussianNB()
    # train_vec, test_vec, train_label, test_label = train_test_split(input_vec, input_lab, train_size=0.666,
    #                                                         stratify=input_lab)

    input_vec = np.array(input_vec)
    input_lab = np.array(input_lab)

    kf = KFold(n_splits=10)
    accuracy_model = []

    for train_index, test_index in kf.split(input_vec):
        X_train, X_test = input_vec[train_index], input_vec[test_index]
        y_train, y_test = input_lab[train_index], input_lab[test_index]

        fitted_model = nb.fit(X_train, y_train)

        accuracy_model.append(accuracy_score(y_test, fitted_model.predict(X_test), normalize=True) * 100)

    # fitted_model = nb.fit(train_vec, train_label)
    #
    # y_pred = fitted_model.predict(test_vec)

    # accuracy = accuracy_score(test_label, y_pred)
    # print("NBC Accuracy: " + "{:.1%}".format(float(accuracy)))
    print("NBC: " + str(np.array(accuracy_model).mean()))

    return 0


def decision_tree_classify(input_vec, input_lab):
    dtc = DecisionTreeClassifier()

    # train_vec, test_vec, train_label, test_label = train_test_split(input_ve
    # c, input_lab, train_size=0.666, stratify=input_lab)

    input_vec = np.array(input_vec)
    input_lab = np.array(input_lab)

    kf = KFold(n_splits=10)
    # kf.get_n_splits(input_vec)

    accuracy_model = []

    for train_index, test_index in kf.split(input_vec):
        X_train, X_test = input_vec[train_index], input_vec[test_index]
        y_train, y_test = input_lab[train_index], input_lab[test_index]

        fitted_model = dtc.fit(X_train, y_train)

        accuracy_model.append(accuracy_score(y_test, fitted_model.predict(X_test), normalize=True) * 100)

        # y_pred = fitted_model.predict(test_vec)
        #
        # accuracy = accuracy_score(test_label, y_pred)
        # print("DTC Accuracy: " + "{:.1%}".format(float(accuracy)))
    print("DTC: " + str(np.array(accuracy_model).mean()))
    # kf = KFold(n_splits=10)
    # kf.get_n_splits(input_vec)

    return 0


def gp_transform(input_vec, input_lab):
    function_set = ["add", "sub", "mul", "div", "sqrt", "log", "abs", "neg", "inv", "max", "min", "sin", "cos", "tan"]
    gpt = SymbolicTransformer(
        population_size=1024,
        generations=20,
        stopping_criteria=0.0001,
        p_crossover=0.7,
        p_subtree_mutation=0.1,
        p_hoist_mutation=0.08,
        p_point_mutation=0.1,
        max_samples=0.9,
        verbose=1,
        parsimony_coefficient=0.06,
        random_state=0,
        function_set=function_set,
        metric="spearman",
    )
    gpt.fit(input_vec, input_lab)
    print(gpt._best_programs[0])
    print(gpt._best_programs[1])
    print(gpt._best_programs[2])
    return gpt.transform(input_vec)


if __name__ == '__main__':
    x, y = read_data("/home/dynehayd/PycharmProjects/Comp422Project2/Data/uci/balance.data")
    print("Balance Data")
    decision_tree_classify(x, y)
    naive_bayes_classify(x, y)
    tx = gp_transform(x, y)
    decision_tree_classify(tx, y)
    naive_bayes_classify(tx, y)
    print("===================================================================")
    x, y = read_data("/home/dynehayd/PycharmProjects/Comp422Project2/Data/uci/wine.data")
    print("Wine Data")
    decision_tree_classify(x, y)
    naive_bayes_classify(x, y)
    tx = gp_transform(x, y)
    decision_tree_classify(tx, y)
    naive_bayes_classify(tx, y)
