# TAKEN FROM https://pyswarms.readthedocs.io/en/latest/examples/tutorials/basic_optimization.html
# import modules
import numpy as np
# from pyswarms.single.global_best import GlobalBestPSO
import pyswarms as ps
from pyswarms.utils.functions import single_obj as fx
import yaml


# def rosenbrock_with_args(x, a, b, c=0):
#     f = (a - x[:, 0]) ** 2 + b * (x[:, 1] - x[:, 0] ** 2) ** 2 + c
#     return f

# instatiate the optimizer
x_max = 30
x_min = -30
bounds = (x_min, x_max)
options = {'c1': 0.5, 'c2': 0.3, 'w': 0.9}
optimizer = ps.single.GlobalBestPSO(
    n_particles=100,
    dimensions=20,
    options=options,
    bounds=(-30, 30),
    init_pos=(0, 0)
)

# now run the optimization, pass a=1 and b=100 as a tuple assigned to args

cost, pos = optimizer.optimize(fx.rosenbrock, 1000)

# create a parameterized version of the classic Rosenbrock unconstrained optimzation function
