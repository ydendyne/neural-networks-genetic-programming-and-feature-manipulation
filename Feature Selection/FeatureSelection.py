import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.metrics import accuracy_score
from gplearn.genetic import SymbolicTransformer
import numpy as np
from sklearn.feature_selection import SelectKBest, chi2
import heapq
from mlxtend.feature_selection import SequentialFeatureSelector as sfs


# following two functions taken from https://www.python-course.eu/Decision_Trees.php
def entropy(target_col):
    """
    Calculate the entropy of a dataset.
    The only parameter of this function is the target_col parameter which specifies the target column
    """
    elements, counts = np.unique(target_col, return_counts=True)
    entropy = np.sum(
        [(-counts[i] / np.sum(counts)) * np.log2(counts[i] / np.sum(counts)) for i in range(len(elements))])
    return entropy


def InfoGain(data, split_attribute_name, target_name="class"):
    """
    Calculate the information gain of a dataset. This function takes three parameters:
    1. data = The dataset for whose feature the IG should be calculated
    2. split_attribute_name = the name of the feature for which the information gain should be calculated
    3. target_name = the name of the target feature. The default for this example is "class"
    """
    # Calculate the entropy of the total dataset
    total_entropy = entropy(data[target_name])

    ##Calculate the entropy of the dataset

    # Calculate the values and the corresponding counts for the split attribute
    vals, counts = np.unique(data[split_attribute_name], return_counts=True)

    # Calculate the weighted entropy
    weighted_Entropy = np.sum(
        [(counts[i] / np.sum(counts)) * entropy(data.where(data[split_attribute_name] == vals[i]).dropna()[target_name])
         for i in range(len(vals))])

    # Calculate the information gain
    information_gain = total_entropy - weighted_Entropy
    return information_gain


def chi_sqr(x_, y_):
    x_new = SelectKBest(chi2, k=5).fit_transform(x_, y_)
    return x_new


def read_data(file_name):
    if "wbcd" in file_name:
        data = pd.read_csv(file_name, header=None, usecols=[i for i in range(31)])
        vec = pd.read_csv(file_name, header=None, usecols=[i for i in range(30)])
        lab = pd.read_csv(file_name, header=None)
        lab = lab[30]
        return vec, lab, data
    elif "sonar" in file_name:
        data = pd.read_csv(file_name, header=None, usecols=[i for i in range(61)])
        vec = pd.read_csv(file_name, header=None, usecols=[i for i in range(60)])
        lab = pd.read_csv(file_name, header=None)
        lab = lab[60]
        return vec, lab, data
    else:
        print("Incorret file")


def naive_bayes_classify(input_vec, input_lab):
    nb = GaussianNB()
    train_vec, test_vec, train_label, test_label = train_test_split(input_vec, input_lab, train_size=0.666,
                                                            stratify=input_lab)
    fitted_model = nb.fit(train_vec, train_label)

    y_pred = fitted_model.predict(test_vec)

    accuracy = accuracy_score(test_label, y_pred)
    print("NBC Accuracy: " + "{:.1%}".format(float(accuracy)))


def seq_feat_sel(input_vec, input_lab):
    train_vec, test_vec, train_label, test_label = train_test_split(input_vec, input_lab, train_size=0.666,
                                                                    stratify=input_lab)
    nb = GaussianNB()
    sfs1 = sfs(nb,
               k_features=5,
               forward=True,
               floating=False,
               verbose=2,
               scoring='accuracy',
               cv=5
               )
    sfs1 = sfs1.fit(train_vec, train_label)
    feat = list(sfs1.k_feature_idx_)
    nbc = GaussianNB()

    fitted_model = nb.fit(train_vec[feat], train_label)

    y_pred = fitted_model.predict(test_vec[feat])
    accuracy = accuracy_score(test_label, y_pred)
    print("NBC Accuracy: " + "{:.1%}".format(float(accuracy)))
    return 0


if __name__ == '__main__':
    k = 5
    x, y, data = read_data("/home/dynehayd/PycharmProjects/Comp422Project2/Data/uci/wbcd.data")
    print("Wisconsin Breast Cancer Data")
    naive_bayes_classify(x, y)
    x_n = chi_sqr(x, y)
    naive_bayes_classify(x_n, y)
    info_gain = []
    for i in range(30):
        info_gain.append(InfoGain(data, i, 30))
    n_largest = heapq.nlargest(k, range(len(info_gain)), info_gain.__getitem__)
    # for j in range(len(n_largest)):
    data = data[n_largest]
    naive_bayes_classify(data, y)
    x, y, data = read_data("/home/dynehayd/PycharmProjects/Comp422Project2/Data/uci/sonar.data")
    print("Sonar Data")
    naive_bayes_classify(x, y)
    x_n = chi_sqr(x, y)
    naive_bayes_classify(x_n, y)
    info_gain = []
    for i in range(60):
        info_gain.append(InfoGain(data, i, 60))
    n_largest = heapq.nlargest(k, range(len(info_gain)), info_gain.__getitem__)
    # for j in range(len(n_largest)):
    data = data[n_largest]
    naive_bayes_classify(data, y)
    print("==========================================")
    x, y, data = read_data("/home/dynehayd/PycharmProjects/Comp422Project2/Data/uci/wbcd.data")
    print("Wisconsin Breast Cancer Data")
    seq_feat_sel(x, y)
    x, y, data = read_data("/home/dynehayd/PycharmProjects/Comp422Project2/Data/uci/sonar.data")
    print("Sonar Data")
    seq_feat_sel(x, y)
